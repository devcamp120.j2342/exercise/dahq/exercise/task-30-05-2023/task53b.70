import java.util.Date;

import model.Customer;
import model.Visit;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer("Nguyen Van A", false, "advanced");
        Customer customer2 = new Customer("Nguyen Van B", false, "member");

        Visit visit1 = new Visit(customer1, new Date(), 20.5, 56.8);

        Visit visit2 = new Visit(customer2, new Date(), 100, 90.9);

        System.out.println("visit1: " + visit1.toString());
        System.out.println("visit2: " + visit2.toString());
    }
}
