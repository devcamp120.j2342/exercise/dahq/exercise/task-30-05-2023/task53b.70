package model;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Visit {
    private Customer customer;
    private Date date;
    private double serviceExpense;
    private double productExpense;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public double getTotalExpense() {
        return this.serviceExpense + this.productExpense;
    }

    public Visit(Customer customer, Date date, double serviceExpense, double productExpense) {
        this.customer = customer;
        this.date = date;
        this.serviceExpense = serviceExpense;
        this.productExpense = productExpense;
    }

    @Override
    public String toString() {

        // định dạng cho ngày tháng
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defaultTimeFormatter = DateTimeFormatter.ofPattern(pattern);

        // return trả ra chuỗi String

        return "Visit[" + customer.toString() + " ,service: " + this.serviceExpense + " ,product: "
                + this.productExpense +
                " ,Date: "
                + defaultTimeFormatter.format(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()) + "]";
    }

}
